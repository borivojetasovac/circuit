﻿"use strict"

function Line(drawing, fp1, fp2) {          // da li da prima drawing ili samo style?

    LineBase.apply(this, arguments);

    this.element_key = "element_key:line";
    this.elements = [];
    
    if (typeof this.render != "function") {

        Line.prototype.render = function (canvas, lines) {

            if (this.elements.length === 0) {
                this.renderPlainLine(canvas, lines);
                return;
            }

            this.elements.forEach(function (element, index, array) {
                element.render(canvas, lines);
            });

            lines.push(this);
        };
        Line.prototype.renderPlainLine = function (canvas, lines) {

            lines.push(this);

            this.activate();

            canvas.add(this.f_line);
            this.hats[1].render(canvas);
            this.hats[2].render(canvas);

            this.deActivate("last deactivation");

            console.log("line added ");
        };
        Line.prototype.unRender = function (canvas, lines) {

            this.activate();
            canvas.remove(this.f_line); // ovo je bez efekta ukoliko je f_line vec ranije otputovala
            this.deActivate("last deactivation");
          
            for (var i = 0; i < lines.length; i++) {
                if (this === lines[i]) {
                    break;
                }
            }

            if (i < lines.length) {
                for (var j = i; j < lines.length - 1; j++) {
                    lines[j] = lines[j + 1];
                }

                lines.pop();
                console.log("line removed ");
            }

            this.elements.forEach(function (line_element, index, array) {
                line_element.unRender(canvas); // lines je dakle undefined u ovoom pozivu
            });

            var neighbours = this.findTwoNeighbours(lines);

            neighbours.forEach(function (neighbour, index, array) {
                // i sad bi ovde islo unrender pa onda add line, ali to ne moze za sada....
            });
        };

        Line.prototype.findTwoNeighbours = function (lines) {
            var neighbours = [], i;

            for (i = 0; i < lines.length; i++) {
                if (samePoint(this.fp1, lines[i].fp1) || samePoint(this.fp1, lines[i].fp2)) {
                    neighbours.push(lines[i]);
                    break;
                }
            }

            for (i = 0; i < lines.length; i++) {
                if (samePoint(this.fp2, lines[i].fp1) || samePoint(this.fp2, lines[i].fp2)) {
                    neighbours.push(lines[i]);
                    break;
                }
            }

            return neighbours;
        };

        Line.prototype.wantsToSnap = function (fp, cellSize) {
            var line = this.getPerpendicular(fp),
                crossing_point = this.crossExactly(line),
                d = distance(fp, crossing_point),
                d1 = distance(this.fp1, crossing_point),
                d2 = distance(this.fp2, crossing_point);

            if (this.contains(crossing_point) && d <= cellSize / 3 && d1 >= cellSize && d2 >= cellSize) { // todo - kasnije mora da dodje i provera na postojece pravougaonike
                var dummy = new DummyRectangle(this, crossing_point);
                return dummy;
            }
            else
                return false;
        };
        Line.prototype.accepts = function (new_element) {
            
            var parent_line = new_element.parent_line,
                center = new_element.center,
                height = new_element.height,
                line_elements = parent_line.elements,
                extra_room_factor = drawing.drawingStyle.extra_room_factor,
                modified_height = height * extra_room_factor,
                canvas = drawing.canvas;

            console.log("parent_line === this -> " + (parent_line === this));
            parent_line.activate();
            parent_line.deActivate("last deactivation");

            if (line_elements.length === 0)
                return true;

            for (var i = 0; i < line_elements.length; i++) {
                var line_element = line_elements[i];

                line_element.activate();
                line_element.deActivate("last deactivation");

                if (line_element instanceof LightLine && line_element.containsInTheMiddle(center)) {
                    var bp1 = new_element.getBoundaryPoint("to fp1", modified_height),
                        bp2 = new_element.getBoundaryPoint("to fp2", modified_height);

                    if (line_element.containsInTheMiddle(bp1) && line_element.containsInTheMiddle(bp2))
                        return true;
                }
            }

            parent_line.drawOffense(new_element);

            return false;
        };

        Line.prototype.addElementToSelf = function (new_element, no_render) {

            if (this.elements.length === 0) {

                this.addElementToEmptySelf(new_element, no_render);
                return;
            }

            this.addElementToNonEmptySelf(new_element, no_render);

        };
        Line.prototype.addElementToEmptySelf = function (new_element, no_render) {
            var bp1 = new_element.getBoundaryPoint("to fp1"),
                bp2 = new_element.getBoundaryPoint("to fp2");

            this.fp1.hasHat = this.fp2.hasHat = true;

            var wire_1 = new LightLine(drawing, this.fp1, bp1),
                wire_2 = new LightLine(drawing, this.fp2, bp2);

            if (!no_render) {
                this.unRender(drawing.canvas, drawing.lines);
            }

            this.elements.push(wire_1, new_element, wire_2);

            if (!no_render) {
                this.render(drawing.canvas, drawing.lines);
            }
        };
        Line.prototype.addElementToNonEmptySelf = function (new_element, no_render) {
            var parent_line = new_element.parent_line,
               center = new_element.center,
               line_elements = parent_line.elements, 
               light_line;

            for (var i = 0; i < line_elements.length; i++) {
                light_line = line_elements[i];

                if (light_line instanceof LightLine && light_line.containsInTheMiddle(center)) {
                    var bp1 = new_element.getBoundaryPoint("to fp1"),
                        bp2 = new_element.getBoundaryPoint("to fp2"),

                        wire_1 = new LightLine(drawing, light_line.fp1, bp1),
                        wire_2 = new LightLine(drawing, light_line.fp2, bp2);

                    if (!no_render) {
                        this.unRender(drawing.canvas, drawing.lines);
                    }

                    this.removeFromElements(light_line);
                    this.elements.push(wire_1, new_element, wire_2);

                    if (!no_render) {
                        this.render(drawing.canvas, drawing.lines);
                    }

                    return;
                }
            }
        };

        Line.prototype.removeFromElements = function (element) {

            for (var i = 0; i < this.elements.length; i++) {
                if (element === this.elements[i]) {
                    break;
                }
            }

            if (i < this.elements.length) {
                for (var j = i; j < this.elements.length - 1; j++) {
                    this.elements[j] = this.elements[j + 1];
                }

                this.elements.pop();
            }

        };
             
        Line.prototype.selfDivideByEndPoints = function (lines) {
            var noodles = [], new_line = this, points = [];
            lines.forEach(function (line, index, array) {
                new_line.activate("dashed");
                line.activate();

                var point = new_line.crossRounded(line);

                if (new_line.containsInTheMiddle(point) && line.hasEndingPoint(point)) {
                    points.push(point);
                }

                new_line.deActivate();
                line.deActivate("last deactivation");
            });

            if (points.length !== 0) {

                points.push(new_line.fp1);
                points.push(new_line.fp2);

                var pivot = new_line.getPivot();
                points.sort(function (l, r) {
                    return distance(pivot, l) - distance(pivot, r);
                });

                for (var i = 0; i < points.length - 1; i++) {                    
                    noodles.push(new Line(drawing, points[i], points[i + 1]));
                }
            }

            return noodles;
        };
        Line.prototype.divideVictims = function (lines) {
            var orphans = [], new_line = this, victims = [];

            lines.forEach(function (line, index, array) {
                new_line.activate("dashed");
                line.activate();

                var point = new_line.crossRounded(line);

                if (line.containsInTheMiddle(point) && new_line.hasEndingPoint(point)) {
                    line.dividingPoint = point;
                    victims.push(line);
                }

                new_line.deActivate();
                line.deActivate("last deactivation");
            });

            victims.forEach(function (victim, index, array) {
                var orphan_1 = new Line(drawing, victim.fp1, victim.dividingPoint),
                    orphan_2 = new Line(drawing, victim.fp2, victim.dividingPoint);

                    orphan_1.victim = orphan_2.victim = victim;
                    orphan_1.skip_me_as_a_sibling = orphan_2.skip_me_as_a_sibling = true;

                orphans.push(orphan_1);
                orphans.push(orphan_2);

                // i sad bi ovde trebalo zvati addElement sa rectangle-ovima od victim-a, ali to je u Drawing-u i ovde se ne vidi....
                // tema za razmisljanje - da li organizovati stvari drugacije da bi to ovde moglo da se pozove ili ne...
                // za sada se orphans vracaju kao i dosad i na njih dodaju elementi stare linije na starom mestu - u addElement
            });

            return orphans;
        };
        Line.prototype.findSiblings = function (lines) {
            var siblings = [], new_line = this;

            lines.forEach(function (line, index, array) {
                new_line.activate("dashed");
                line.activate();

                if (!new_line.skip_me_as_a_sibling && new_line.onSameStraightLine(line) && new_line.hasSingleCommonEndingPoint(lines, line)) {
                    siblings.push(line);
                }

                new_line.deActivate();
                line.deActivate("last deactivation");
            });

            return siblings;
        };
        Line.prototype.makeBigLine = function (siblings, inheritElements) {
            var new_line = this, big_line, points;

            points = new_line.lineUp(siblings[0]);
            big_line = new Line(drawing, points[0].point, points[3].point);
            //inheritElements(heir, predecessor);
            inheritElements(big_line, siblings[0]);

            if (siblings.length > 1) {
                points = big_line.lineUp(siblings[1]);
                big_line = new Line(drawing, points[0].point, points[3].point);
                //inheritElements(heir, predecessor);
                inheritElements(big_line, siblings[0]);
                inheritElements(big_line, siblings[1]);
            }

            return big_line;
        };

        Line.prototype.drawOffense = function (rectangle) {
            var heap = [], fll, 
                canvas = rectangle.parent_line.drawing.canvas;

            if (this.elements.length !== 0) {
                this.elements.forEach(function (item, index, array) {

                    item.activate();
                    item.deActivate("last deactivation");

                    if (item instanceof LightLine && item.crosses(rectangle)) {
                        fll = new fabric.Line([item.fp1.x, item.fp1.y, item.fp2.x, item.fp2.y], {
                            stroke: "blue",
                            strokeDashArray: [3, 3],
                            strokeWidth: 2,
                            selectable: false
                        });

                        heap.push(fll);
                        canvas.add(fll);
                    }

                    if (item instanceof Rectangle && item.overlaps(rectangle)) {
                        item.extended_rectangle.forEach(function (side, index, array) {
                            var fside = new fabric.Line([side.fp1.x, side.fp1.y, side.fp2.x, side.fp2.y], {
                                stroke: "blue",
                                strokeDashArray: [3, 3],
                                strokeWidth: 2,
                                selectable: false
                            });

                            heap.push(fside);
                            canvas.add(fside);
                        });
                    }
                });
            }
            else {
                fll = new fabric.Line([this.fp1.x, this.fp1.y, this.fp2.x, this.fp2.y], {
                    stroke: "blue",
                    strokeDashArray: [3, 3],
                    strokeWidth: 2,
                    selectable: false
                });

                heap.push(fll);
                canvas.add(fll);
            }

            rectangle.extended_rectangle.forEach(function (item, index, array) {
                var fitem = new fabric.Line([item.fp1.x, item.fp1.y, item.fp2.x, item.fp2.y], {
                    stroke: "pink",
                    strokeWidth: 2,
                    selectable: false
                });

                heap.push(fitem);
                canvas.add(fitem);
            });

            setTimeout(function () {
                heap.forEach(function (item, index, array) {
                    canvas.remove(item);
                });
            }, 500);
        };
        Line.prototype.checkoutLinesOnErasing = function (critical_point) {

            if (this.elements.length === 0) {
                if (this.contains(critical_point))
                    return this;
                else
                    return false;
            }
            else {
                for (var i = 0; i < this.elements.length; i++) {
                    var element = this.elements[i];

                    if (element instanceof LightLine && element.contains(critical_point))
                        return this;
                }
            }

            return false;
        };        
        Line.prototype.checkoutRectanglesOnErasing = function (critical_point) {

            if (this.elements.length === 0) {                
                    return false;
            }
            else {
                for (var i = 0; i < this.elements.length; i++) {
                    var element = this.elements[i];

                    if (element instanceof Rectangle && element.contains(critical_point))
                        return element;
                }
            }

            return false;
        };
        
    }
}

inheritPrototype(Line, LineBase);
