﻿"use strict"

function LineBase(drawing, fp1, fp2) {

    Scribble.call(this, drawing);
       
    this.fp1 = this.fp2 = this.k = this.n = undefined; // this.setPoints(fp1, fp2); im daje vrednosti!!!

    this.f_line = new fabric.Line([fp1.x, fp1.y, fp2.x, fp2.y], {
        stroke: drawing.drawingStyle.line_color,
        strokeWidth: drawing.drawingStyle.line_width,
        selectable: false
    });
    this.hats = [undefined, new Hat(drawing, fp1), new Hat(drawing, fp2)];

    if (typeof this.isUnder != "function") {

        LineBase.prototype.setPoints = function (fp1, fp2) {
            this.fp1 = fp1;
            this.fp2 = fp2;

            this.k = (fp2.y - fp1.y) / (fp2.x - fp1.x);
            this.n = fp1.y - this.k * fp1.x;

            var pivot = this.getPivot();

            if (distance(fp1, pivot) > distance(fp2, pivot)) {
                this.fp1 = fp2;
                this.fp2 = fp1;
            }
        };

        LineBase.prototype.isUnder = function (other_line) {

            if (this.k !== other_line.k)
                return false;

            if (this.parallel(other_line))
                return false;

            // podudarni pravci !!!
            var points = this.lineUp(other_line);

            if (points[0].ord === 0 && points[1].ord === 1 && points[2].ord === 2 && points[3].ord === 3) // .........pivot...........0____1......2____3.........
                return false;

            if (points[0].ord === 2 && points[1].ord === 3 && points[2].ord === 0 && points[3].ord === 1) // .........pivot...........2____3......0____1.........
                return false;

            if (points[1].radius === points[2].radius) // .........pivot...........2____03____1.........
                return false;

            return true;
        };
        LineBase.prototype.lineUp = function (other_line) {
            var points = [], i = 0,
                pivot = this.getPivot();

            points.push({ ord: i++, radius: distance(this.fp1, pivot), point: this.fp1 });
            points.push({ ord: i++, radius: distance(this.fp2, pivot), point: this.fp2 });
            points.push({ ord: i++, radius: distance(other_line.fp1, pivot), point: other_line.fp1 });
            points.push({ ord: i++, radius: distance(other_line.fp2, pivot), point: other_line.fp2 });

            return stable(points, function (l, r) { // u stvari ne mora stable
                return l.radius - r.radius;
            });
        };
        LineBase.prototype.getPivot = function () {
            if (this.vertical()) {
                return { x: this.fp1.x, y: -1 };
            }

            return { x: -1, y: this.k * -1 + this.n };
        };
        LineBase.prototype.length = function () {
            return distance(this.fp1, this.fp2);
        };

        LineBase.prototype.vertical = function () {
            if (this.k === Infinity || this.k === -Infinity)
                return true;
            else
                return false;
        };
        LineBase.prototype.horizontal = function () {
            if (this.k === 0)
                return true;
            else
                return false;
        };
        LineBase.prototype.parallel = function (other_line) {

            if (this.vertical() && other_line.vertical()) {
                if (this.fp1.x !== other_line.fp1.x)
                    return true;
                else
                    return false;
            }

            if (this.k === other_line.k && this.n !== other_line.n)
                return true;

            return false;
        };
        LineBase.prototype.getPerpendicular = function (fp) {
            if (this.vertical()) {
                return new LightLine(drawing, { x: fp.x - 3000, y: fp.y }, { x: fp.x + 3000, y: fp.y });
            }

            if (this.horizontal()) {
                return new LightLine(drawing, { x: fp.x, y: fp.y - 3000 }, { x: fp.x, y: fp.y + 3000 });
            }

            var k = (-1) / this.k,
                n = fp.y - k * fp.x,
                x_temp1 = fp.x + 3000,
                point1 = { x: x_temp1, y: k * x_temp1 + n },
                x_temp2 = fp.x - 3000, 
                point2 = { x: x_temp2, y: k * x_temp2 + n };

            return new LightLine(drawing, point1, point2);
        };

        LineBase.prototype.hasEndingPoint = function (fp) {
            if (samePoint(fp, this.fp1) || samePoint(fp, this.fp2))
                return true;

            return false;
        };
        LineBase.prototype.commonEndingPoint = function (other_line) {
            if (this.endsWith(other_line.fp1))
                return other_line.fp1;

            if (this.endsWith(other_line.fp2))
                return other_line.fp2;

            return false;
        };
        LineBase.prototype.hasSingleCommonEndingPoint = function (lines, other_line) {
            if (!this.hasCommonEndingPoint(other_line))
                return false;

            var point = this.commonEndingPoint(other_line);
            var isSingle = true;
            lines.forEach(function (line, index, array) {
                if (line !== other_line) {
                    if (line.endsWith(point))
                        isSingle = false;
                }
            });

            return isSingle;
        };
        LineBase.prototype.hasCommonEndingPoint = function (other_line) {
            if (samePoint(this.fp1, other_line.fp1) || samePoint(this.fp1, other_line.fp2))
                return true;

            if (samePoint(this.fp2, other_line.fp1) || samePoint(this.fp2, other_line.fp2))
                return true;

            return false;
        };
        LineBase.prototype.endsWith = function (fp) {
            if (this.fp1.x === fp.x && this.fp1.y === fp.y)
                return fp;

            if (this.fp2.x === fp.x && this.fp2.y === fp.y)
                return fp;

            return false;
        };

        LineBase.prototype.onSameStraightLine = function (other_line) {
            if (this.vertical() && other_line.vertical()) {
                if (this.parallel(other_line))
                    return false;
                else
                    return true;
            }

            if (this.k === other_line.k && this.n === other_line.n)
                return true;

            return false;
        };
        LineBase.prototype.getSpecificPoint = function (starting_point, leap, direction) {
            if (!this.containsInTheMiddle(starting_point))
                return false;

            if (direction === "to fp1" && this.vertical()) {
                if (this.fp1.y > starting_point.y)
                    return { x: this.fp1.x, y: starting_point.y + leap };
                else
                    return { x: this.fp1.x, y: starting_point.y - leap };
            }

            if (direction === "to fp1") {
                var dx = leap * Math.cos(this.slopeAngle());

                if (this.k >= 0) {
                    if (this.fp1.x > starting_point.x)
                        return { x: starting_point.x + dx, y: this.k * (starting_point.x + dx) + this.n };
                    else
                        return { x: starting_point.x - dx, y: this.k * (starting_point.x - dx) + this.n };
                }
                else {
                    if (this.fp1.x < starting_point.x)
                        return { x: starting_point.x + dx, y: this.k * (starting_point.x + dx) + this.n };
                    else
                        return { x: starting_point.x - dx, y: this.k * (starting_point.x - dx) + this.n };
                }
            }

            if (direction === "to fp2" && this.vertical()) {
                if (this.fp2.y > starting_point.y)
                    return { x: this.fp2.x, y: starting_point.y + leap };
                else
                    return { x: this.fp2.x, y: starting_point.y - leap };
            }

            if (direction === "to fp2") {
                var dx = leap * Math.cos(this.slopeAngle());

                if (this.k >= 0) {
                    if (this.fp2.x > starting_point.x)
                        return { x: starting_point.x + dx, y: this.k * (starting_point.x + dx) + this.n };
                    else
                        return { x: starting_point.x - dx, y: this.k * (starting_point.x - dx) + this.n };
                }
                else {
                    if (this.fp2.x < starting_point.x)
                        return { x: starting_point.x + dx, y: this.k * (starting_point.x + dx) + this.n };
                    else
                        return { x: starting_point.x - dx, y: this.k * (starting_point.x - dx) + this.n };
                }
            }

            return false;
        };
        LineBase.prototype.angleBetween = function (other_line) {
            if (this.vertical() && other_line.vertical())
                return 0;

            var line, angle;
            if (this.vertical()) {
                line = this.getPerpendicular({ x: this.fp1.x + 100, y: this.fp1.y });
                angle = line.angleBetween(other_line);
                return Math.PI / 2 - angle;
            }

            if (other_line.vertical()) {
                line = other_line.getPerpendicular({ x: other_line.fp1.x + 100, y: other_line.fp1.y });
                angle = line.angleBetween(this);
                return Math.PI / 2 - angle;
            }

            angle = Math.atan(Math.abs((this.k - other_line.k) / (1 + this.k * other_line.k)));

            return angle;
        };
        LineBase.prototype.slopeAngle = function () {
            if (this.k >= 0)
                return Math.atan(this.k);
            else
                return Math.atan(this.k) + Math.PI;
        };

        LineBase.prototype.crossRounded = function (other_line) {
            if (this.parallel(other_line))
                return false;

            if (this.vertical() && other_line.vertical()) // linije su na istoj pravoj, ako se dodje dovde znaci da nisu paralelne
                return false;

            if (this.vertical() && !other_line.vertical())
                return { x: this.fp1.x, y: Math.round(other_line.k * this.fp1.x + other_line.n) };

            if (other_line.vertical() && !this.vertical())
                return { x: other_line.fp1.x, y: Math.round(this.k * other_line.fp1.x + this.n) };

            var x_cross = Math.round((this.n - other_line.n) / (other_line.k - this.k));
            if (isNaN(x_cross)) // linije su na istoj pravoj koja nije vertikalna
                return false;

            return { x: x_cross, y: Math.round(this.k * x_cross + this.n) };
        };
        LineBase.prototype.crossExactly = function (other_line) {
            if (this.parallel(other_line))
                return false;

            if (this.vertical() && other_line.vertical()) // linije su na istoj pravoj, ako se dodje dovde znaci da nisu paralelne
                return false;

            if (this.vertical() && !other_line.vertical())
                return { x: this.fp1.x, y: other_line.k * this.fp1.x + other_line.n };

            if (other_line.vertical() && !this.vertical())
                return { x: other_line.fp1.x, y: this.k * other_line.fp1.x + this.n };

            var x_cross = (this.n - other_line.n) / (other_line.k - this.k);
            if (isNaN(x_cross)) // linije su na istoj pravoj koja nije vertikalna
                return false;

            return { x: x_cross, y: this.k * x_cross + this.n };
        };
        LineBase.prototype.crosses = function (rectangle) {

            var extended_rectangle = rectangle.extended_rectangle,
                this_line = this, 
                flag = false;

                extended_rectangle.forEach(function (light_line, index, array) {

                    var point = this_line.crossExactly(light_line);

                    if (point && light_line.contains(point) && this_line.contains(point)) {
                        flag = true;
                    }
                });

            return flag;
        };
        LineBase.prototype.contains = function (fp) {
            if (this.vertical()) {
                if (!closeEnough(fp.x, this.fp1.x))
                    return false;
                else if (fp.y >= min(this.fp1.y, this.fp2.y) && fp.y <= max(this.fp1.y, this.fp2.y))
                    return true;
                else
                    return false;
            }

            if (this.horizontal()) {
                if (!closeEnough(fp.y, this.fp1.y))
                    return false;
                else if (fp.x >= min(this.fp1.x, this.fp2.x) && fp.x <= max(this.fp1.x, this.fp2.x))
                    return true;
                else
                    return false;
            }

            var temp_y = this.k * fp.x + this.n,
                y1 = this.k * this.fp1.x + this.n,
                y2 = this.k * this.fp2.x + this.n;

            if (!closeEnough(temp_y, fp.y)) // da li temp_y lezi na pravoj, tj. da li ne lezi
                return false;
            else if (temp_y >= min(y1, y2) && temp_y <= max(y1, y2))
                return true;
            else
                return false;
        };
        LineBase.prototype.containsInTheMiddle = function (fp) {
            if (this.contains(fp)) {
                if (samePoint(fp, this.fp1))
                    return false;
                if (samePoint(fp, this.fp2))
                    return false;

                return true;
            }

            return false;
        };        
    }

    this.setPoints(fp1, fp2); // mora skroz na kraju, da bi se prvi put proslo kroz if
}

inheritPrototype(LineBase, Scribble);
