﻿"use strict"

function LightLine(drawing, fp1, fp2) {
    Line.apply(this, arguments);

    this.element_key = "element_key:light_line";

    this.elements = undefined; // za sada dok ne dodju obelezavanja... a mozda ni tada?

    if (typeof this.dummyFunctionDrawingLine != "function") {
        LightLine.prototype.dummyFunctionDrawingLine = function () { };

        LightLine.prototype.render = function (canvas) {

            this.activate();
            canvas.add(this.f_line);
            this.deActivate("last deactivation");

        };
        LightLine.prototype.unRender = function (canvas) {

            this.activate();
            canvas.remove(this.f_line);
            this.deActivate("last deactivation");

        };
    }
}

inheritPrototype(LightLine, Line);
