﻿"use strict"

function Scribble(drawing) {
    
    this.drawing = drawing;

    if (typeof this.activate != "function") {

        Scribble.prototype.activate = function (dashed) {
            if (!this.debug_mode)
                return;

            var f_temp_line = new fabric.Line([this.fp1.x, this.fp1.y, this.fp2.x, this.fp2.y], {
                stroke: "green",
                strokeDashArray: dashed === "dashed" ? [3, 3] : [],
                strokeWidth: 3,
                selectable: false
            });

            this.drawing.canvas.renderOnAddRemove = true;

            this.f_temp_line = f_temp_line;
            this.drawing.canvas.add(f_temp_line);

            this.Ax = this.fp1.x;
            this.Ay = this.fp1.y;
            this.Bx = this.fp2.x;
            this.By = this.fp2.y;
        };
        Scribble.prototype.deActivate = function (lastDeactivation) {

            if (!this.debug_mode)
                return;

            this.drawing.canvas.remove(this.f_temp_line);
            delete this.f_temp_line;

            if (lastDeactivation === "last deactivation")
                canvas.renderOnAddRemove = false;

            delete this.Ax;
            delete this.Ay;
            delete this.Bx;
            delete this.By;
        };
        Scribble.prototype.debug_mode = true;
    }
}