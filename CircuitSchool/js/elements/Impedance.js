﻿"use strict"

function Impedance(dummy_rectangle) {
    Rectangle.call(this, dummy_rectangle);

    this.element_key = "element_key:rectangle";

    this.width = dummy_rectangle.cellSize / 2;
    this.height = dummy_rectangle.cellSize;

    if (typeof this.clone != "function") {
        Impedance.prototype.clone = function () {
            return new Impedance(this.getDummyRectangle());
        };
    }

    this.init(); // populates curves, extended rectangle
}

inheritPrototype(Impedance, Rectangle);
