﻿"use strict"

function Rectangle(dummy_rectangle) {
    this.element_key = "element_key:rectangle";

    this.parent_line = dummy_rectangle.parent_line;
    this.center = dummy_rectangle.center;
    this.cellSize = dummy_rectangle.cellSize;

    this.curves = [];
    this.extended_rectangle = [];

    if (typeof this.getBoundaryPoint != "function") {

        Rectangle.prototype.init = function () {
            this.populateCurves();
            this.populateExtendedRectangle(this.getOwnModifiedHeight());
        };
        Rectangle.prototype.getDummyRectangle = function () {
            var dummy = new DummyRectangle(this.parent_line, this.center);

            dummy.cellSize = this.cellSize;

            return dummy;
        };

        Rectangle.prototype.getOwnModifiedHeight = function () {
            return this.parent_line.drawing.drawingStyle.extra_room_factor * this.height;
        };
        Rectangle.prototype.getBoundaryPoint = function (direction, modified_height) {

            if (!modified_height) 
                return this.parent_line.getSpecificPoint(this.center, this.height / 2, direction);
            else
                return this.parent_line.getSpecificPoint(this.center, modified_height / 2, direction);

        };
        Rectangle.prototype.getTwoCornerPoints = function (direction, modified_height) {

            var bp = this.getBoundaryPoint(direction, modified_height),
                line = this.parent_line.getPerpendicular(bp),
                corners = [],
                corner,
                width;

            if (modified_height) {
                var factor = modified_height / this.height;

                width = this.width * factor;
            }
            else
                width = this.width;


            corner = line.getSpecificPoint(bp, width / 2, "to fp1");
            corners.push(corner);
            corner = line.getSpecificPoint(bp, width / 2, "to fp2");
            corners.push(corner);

            return corners;
        };
        Rectangle.prototype.getCornerPoints = function (modified_height) {

            var corners, temp_cors;

            if (modified_height) {
                corners = this.getTwoCornerPoints("to fp1", modified_height),
                temp_cors = this.getTwoCornerPoints("to fp2", modified_height);
            }
            else {
                corners = this.getTwoCornerPoints("to fp1"),
                temp_cors = this.getTwoCornerPoints("to fp2");
            }

            corners.push(temp_cors[1]);
            corners.push(temp_cors[0]);

            return corners;
        };

        Rectangle.prototype.overlaps = function (rectangle) {

            var this_extended_rectangle = this.extended_rectangle,
                extended_rectangle = rectangle.extended_rectangle,
                flag = false;

            this_extended_rectangle.forEach(function (this_light_line, index, array) {
                extended_rectangle.forEach(function (light_line, index, array) {
                    var point = this_light_line.crossExactly(light_line);
               
                    if (point && light_line.contains(point) && this_light_line.contains(point)) {
                        flag = true;
                    }
                });
            });

            return flag;
        };
        Rectangle.prototype.contains = function (point) {   // todo - svesti na prave mere, ne na extended (krivi elementi komplikuju stvar, mozda uvesti this.rectangle = []; za sve?)
            var p1, p2, p3, p4, d1, d2, d3, d4, d12, d34, line;

            line = this.extended_rectangle[0].getPerpendicular(point);
            p1 = this.extended_rectangle[0].crossExactly(line);
            p2 = this.extended_rectangle[2].crossExactly(line);

            line = this.extended_rectangle[1].getPerpendicular(point);
            p3 = this.extended_rectangle[1].crossExactly(line);
            p4 = this.extended_rectangle[3].crossExactly(line);

            d1 = distance(p1, point);
            d2 = distance(p2, point);
            d3 = distance(p3, point);
            d4 = distance(p4, point);

            d12 = distance(p1, p2);
            d34 = distance(p3, p4);

            if (d1 <= d12 && d2 <= d12 && d3 <= d34 && d4 <= d34)
                return true;
            else
                return false;
        };

        Rectangle.prototype.populateCurves = function () {

            var cors = this.getCornerPoints(),
                drawing = this.parent_line.drawing;

            for (var i = 0; i < cors.length - 1; i++) {
                var line = new LightLine(drawing, cors[i], cors[i + 1]);
                
                this.curves.push(line);
            }

            this.curves.push(new LightLine(drawing, cors[3], cors[0]));

        };
        Rectangle.prototype.populateExtendedRectangle = function (modified_height) {

            var cors = this.getCornerPoints(modified_height),
                drawing = this.parent_line.drawing;

            for (var i = 0; i < cors.length - 1; i++) {
                var line = new LightLine(drawing, cors[i], cors[i + 1]);

                this.extended_rectangle.push(line);
            }

            this.extended_rectangle.push(new LightLine(drawing, cors[3], cors[0]));

        };

        Rectangle.prototype.render = function (canvas) {
            this.curves.forEach(function (curve, index, array) {
                curve.render(canvas);
            });
        };
        Rectangle.prototype.unRender = function (canvas) {
            this.curves.forEach(function (curve, index, array) {
                curve.unRender(canvas);
            });
        };

        Rectangle.prototype.activate = function (canvas) {

            this.extended_rectangle.forEach(function (light_line, index, array) {
                light_line.activate();
            });

        };
        Rectangle.prototype.deActivate = function (canvas, lastDeactivation) {
            
            var light_line, i;

            for (i = 0; i < this.extended_rectangle.length - 1; i++) {
                light_line = this.extended_rectangle[i];
                light_line.deActivate();
            }

            light_line = this.extended_rectangle[i];
            light_line.deActivate(canvas, lastDeactivation);
        };
    }
    
}
