﻿"use strict"

function DummyRectangle(line, center) {
    this.element_key = "element_key:dummy_rectangle";

    this.parent_line = line;
    this.center = center;
    this.cellSize = undefined;
}