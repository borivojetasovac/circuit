﻿"use strict"

var canvas = new fabric.Canvas('canvas', { stateful: false });
canvas.selectionColor = "rgba(0, 0, 0, 0.0)";
canvas.selectionBorderColor = "rgba(0, 0, 0, 0.0)";

//         new Drawing(canvas, cellCountX, cellCountY, cellMargin, cellSize, style);
var drawing = new Drawing(canvas, 20, 10, 1, 20, new DrawingStyle());

$("#crtanje_linija").click(function () {
    drawing.mouse_key = "mouse_key:line_drawing";
});
$("#erase").click(function () {
    drawing.mouse_key = "mouse_key:erasing";
});
$("#crtanje_pravougaonika").click(function () {
    drawing.mouse_key = "mouse_key:impedance_drawing";
});
$("#start").click(function () {    
    $("#canvas").css("background-color", "yellow");
});
$("#toggle_debug").click(function () {
    var debug_on = drawing.toggleDebug();
    if (debug_on)
        $("#toggle_debug").text("debug ON");
    else
        $("#toggle_debug").text("debug OFF");
});


$(function () {
    console.log("horde");
    //var horde = gremlins.createHorde()
    //horde.unleash();
});

// todo
// lines, elements, ext_rectangle bi trebalo da budu sopstveni TIPOVI

