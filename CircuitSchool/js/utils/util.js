﻿"use strict"

function samePoint(fp1, fp2) {
    if (fp1.x === fp2.x && fp1.y === fp2.y)
        return true;
    else
        return false;
}
function distance(f_point_1, f_point_2) {
    return Math.sqrt(Math.pow(f_point_1.x - f_point_2.x, 2) + Math.pow(f_point_1.y - f_point_2.y, 2));
}

function closeEnough(num1, num2) {
    // todo - testiraj da li je 1px ok?
    if (Math.abs(num1 - num2) < 1)
        return true;

    return false;
}
function min(num1, num2) {
    return num1 < num2 ? num1 : num2;
}
function max(num1, num2) {
    return num1 > num2 ? num1 : num2;
}

function object(obj) {
    function F() { }
    F.prototype = obj;
    return new F();
}
function inheritPrototype(subType, superType) { // subType inherits superType's prototype
    var prototype = object(superType.prototype); //create object
    prototype.constructor = subType; //augment object
    subType.prototype = prototype; //assign object
}

function SuperType(name){
    this.name = name;
    this.colors = ['red', 'blue', 'green'];
}
SuperType.prototype.sayName = function(){
    alert(this.name);
};

function SubType(name, age){
    SuperType.call(this, name);
    this.age = age;
}

inheritPrototype(SubType, SuperType);
SubType.prototype.sayAge = function(){
    alert(this.age);
};

