﻿"use strict"

function Hat(drawing, fp) {    // moze da bude i trougao, krug npr.
    
    if (typeof this.getSize != "function") {

        Hat.prototype.size = function (cellSize) {
            var size = cellSize / 4;

            return size;
        };
        Hat.prototype.offset = function (cellSize) {
           return 0.5;
        };

        Hat.prototype.render = function (canvas) {
            canvas.add(this.f_rect);
        };
    }

    this.f_rect = new fabric.Rect({
        left: fp.x + this.offset(drawing.cellSize),
        top: fp.y,
        fill: drawing.drawingStyle.hat_color,
        width: this.size(drawing.cellSize),
        height: this.size(drawing.cellSize),
        selectable: false
    });

    //var text = new fabric.Text("123", {
    //    left: fp.x,
    //    top: fp.y,
    //    //stroke: 'white',
    //    strokeWidth: 1,
    //    fontSize: 20,
    //    fill: 'white'
    //});

}