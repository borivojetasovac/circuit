﻿"use strict"

function DrawingStyle() {
    // todo - parametrizovati ovaj konstruktor
    this.dashes_color = 'black';
    this.dashes_width = 1;

    this.line_color = 'red';
    this.line_width = 1;

    this.temp_line_color = 'blue';
    this.temp_line_width = 1;

    this.hat_color = 'black';

    this.extra_room_factor = 1.75;
}