﻿"use strict"

function Drawing(canvas, cellCountX, cellCountY, cellMargin, cellSize, drawingStyle) {

    var drawGrid, snapToGrid, snapToLine, linesAccept, elementsAccept, drawingAccepts, testAgainstLines, testAgainstElements, cleanUp, flipFlop, inheritElements, 
        mouseDown = {}, mouseMove = {}, mouseUp = {}, initMouseHandlers, add = {}, remove = {}, addElement, removeElement,

        self = this, f_dummy_line, fp1, fp2, f_temp_line, lines, is_mouse_down = false, is_flip = true, 
        mouse_key_list = ["mouse_key:null_function", "mouse_key:line_drawing", "mouse_key:erasing", "mouse_key:impedance_drawing"],
        element_key_list = ["element_key:line", "element_key:dummy_rectangle", "element_key:rectangle", "element_key:impedance"];    

    this.mouse_key = "mouse_key:null_function";
    this.toggleDebug = function () {
        var dummy_line = new LightLine(this, { x: 0, y: 0 }, { x: 0, y: 0 }); // samo da probudi prototip u LineBase-u!!!!!!!!!!

        Scribble.prototype.debug_mode = !Scribble.prototype.debug_mode;
        return Scribble.prototype.debug_mode;
    };

    this.width;
    this.height;    // setuje ih drawGrid()
    
    this.lines = [];
    lines = this.lines;    

    this.canvas = canvas;
    this.drawingStyle = drawingStyle;

    this.cellCountX = cellCountX;
    this.cellCountY = cellCountY;
    this.cellMargin = cellMargin;
    this.cellSize = cellSize;

    this.clickRadius = cellSize * 0.71;

    linesAccept = function (rectangle) {
        var line,
            new_extended_rectangle,
            possible = true;

        for (var i = 0; i < lines.length; i++) {

            line = lines[i];

            if (line !== rectangle.parent_line) {

                rectangle.activate();
                line.activate();
                rectangle.deActivate();
                line.deActivate("last deactivation");

                new_extended_rectangle = rectangle.extended_rectangle;

                new_extended_rectangle.forEach(function (light_line, index, array) {
                    var point = line.crossExactly(light_line);

                    line.activate();
                    light_line.activate();
                    line.deActivate();
                    light_line.deActivate("last deactivation");

                    if (point && light_line.contains(point) && line.contains(point)) {
                        line.drawOffense(rectangle);
                        possible = false;
                    }
                });

                if (!possible)
                    return false;
            }
        }

        return true;
    };
    elementsAccept = function (rectangle) {
        var line,
            new_extended_rectangle,
            old_extended_rectangle, 
            possible = true;

        for (var i = 0; i < lines.length; i++) {

            line = lines[i];

            if (line !== rectangle.parent_line) {
                rectangle.activate();
                line.activate();
                rectangle.deActivate();
                line.deActivate("last deactivation");

                new_extended_rectangle = rectangle.extended_rectangle;

                line.elements.forEach(function (element, index, array) {

                    if (element instanceof Rectangle) {

                        old_extended_rectangle = element.extended_rectangle;

                        old_extended_rectangle.forEach(function (old_light_line, index, array) {
                            new_extended_rectangle.forEach(function (new_light_line, index, array) {
                                var point = old_light_line.crossExactly(new_light_line);

                                new_light_line.activate();
                                old_light_line.activate();
                                new_light_line.deActivate();
                                old_light_line.deActivate("last deactivation");

                                if (point && new_light_line.contains(point) && old_light_line.contains(point)) {
                                    line.drawOffense(rectangle);
                                    possible = false;
                                }
                            });
                        });
                    }
                });

                if (!possible)
                    return false;
            }
        }

        return true;
    };
    drawingAccepts = function (rectangle) {
       
        if (linesAccept(rectangle) && elementsAccept(rectangle))
            return true;
        else
            return false;
    };

    testAgainstLines = function (new_line) {

        var line;

        for (var i = 0; i < lines.length; i++) {

            line = lines[i];

            new_line.activate("dashed");
            line.activate();            

            new_line.deActivate();
            line.deActivate("last deactivation");

            if (line.isUnder(new_line))
                return false;
        }

        return true;
    };
    testAgainstElements = function (new_line) {

        var line, elements, 
            possible = true;

        for (var i = 0; i < lines.length; i++) {

            line = lines[i];
            elements = line.elements;

            new_line.activate("dashed");
            line.activate();
            line.deActivate();
            new_line.deActivate("last deactivation");

            elements.forEach(function (element, index, array) {

                if (element instanceof LightLine) {
                    element.activate();
                    element.deActivate("last deactivation");
                }

                if (element instanceof Rectangle) {
                    var extended_rectangle = element.extended_rectangle;

                    element.activate();
                    element.deActivate();

                    extended_rectangle.forEach(function (light_line, index, array) {
                        var point = new_line.crossExactly(light_line);
                        
                        light_line.activate();                        
                        light_line.deActivate("last deactivation");

                        if (point && light_line.contains(point) && new_line.contains(point))
                            possible = false;
                    });
                }
            });

            if (!possible)
                return false;
        }

        return true;
    };

    drawGrid = function () {
        // todo
        //dashes_style.dashes_array  e.g. [1, 19, ...]
        //dashes_style.no_dashes

        var f_temp_line, i,
            x = cellSize * cellMargin,
            y = cellSize * cellMargin,
            dashes = [1];

        for (i = 0; i < cellCountY + 1; i++)
            dashes.push(cellSize - 1, 1);

        self.width = cellSize * cellCountX + 2 * cellMargin * cellSize;
        self.height = cellSize * cellCountY + 2 * cellMargin * cellSize;

        canvas.setWidth(self.width);
        canvas.setHeight(self.height);
        f_dummy_line = new fabric.Line([0, 0, 10, 10], {            
            stroke: $("#canvas").css("background-color"),
            strokeWidth: 1,
            selectable: false
        });

        canvas.add(f_dummy_line);

        for (i = 0; i < cellCountX + 1; i++) {
            f_temp_line = new fabric.Line([x + i * cellSize, y, x + i * cellSize, y + cellSize * cellCountY + 1], {
                strokeDashArray: dashes, // todo - ovo sve parametrizovano
                stroke: drawingStyle.dashes_color,
                strokeWidth: drawingStyle.dashes_width,
                selectable: false
            });

            canvas.add(f_temp_line);
            // todo
            // this.add(f_temp_line); // da bi posle moglo da se brise
        }
    };
    flipFlop = function () {
   
        if (is_flip) {
            canvas.remove(f_dummy_line);         
            is_flip = !is_flip;
        } else {
            canvas.add(f_dummy_line);          
            is_flip = !is_flip;
        }
    };
    cleanUp = function (lines) {
        lines.forEach(function (line, index, array) {

            if (line.skip_me_as_a_sibling) {
                delete line.skip_me_as_a_sibling;
            }

            if (line.victim) {
                delete line.victim;
            }
        });
    };

    snapToGrid = function (point) {
        var i, j, x, y, x_mod, y_mod;

        if (point.x < cellMargin * cellSize - cellSize / 4)
            return false;
        if (point.y < cellMargin * cellSize - cellSize / 4)
            return false;

        if (point.x > cellMargin * cellSize + cellCountX * cellSize + cellSize / 4)
            return false;
        if (point.y > cellMargin * cellSize + cellCountY * cellSize + cellSize / 4)
            return false;

        x_mod = point.x - point.x % cellSize;
        y_mod = point.y - point.y % cellSize;

        for (x = x_mod, i = 0; i < 2; i++, x += cellSize) {
            for (y = y_mod, j = 0; j < 2; j++, y += cellSize) {
                if (Math.pow(x - point.x, 2) + Math.pow(y - point.y, 2) <= Math.pow(self.clickRadius, 2)) {
                    point.x = x;
                    point.y = y;

                    return point;
                }
            }
        }

        return false;
    };
    snapToLine = function (point) {
        var dummy_rectangle;
        
        for (var i = 0; i < lines.length; i++) {
            var line = lines[i];

            line.activate();
            line.deActivate("last deactivation");

            dummy_rectangle = line.wantsToSnap(point, cellSize);
            if (dummy_rectangle) {                
                break;
            }
        }

        if (dummy_rectangle)
            return dummy_rectangle;

        return false;
    };

    initMouseHandlers = function () {
        mouseDown["mouse_key:null_function"] = function (options) { };
        mouseMove["mouse_key:null_function"] = function (options) { };
        mouseUp["mouse_key:null_function"] = function (options) { };

        mouseDown["mouse_key:line_drawing"] = function (options) {

            canvas.renderOnAddRemove = true;
            flipFlop();

            fp1 = canvas.getPointer(options.e);

            fp1 = snapToGrid(fp1);

            if (fp1)
                is_mouse_down = true;
        };
        mouseMove["mouse_key:line_drawing"] = function (options) {
            if (is_mouse_down) {
                fp2 = canvas.getPointer(options.e);

                fp2 = snapToGrid(fp2);

                if (fp2 && !samePoint(fp1, fp2)) {
                    canvas.remove(f_temp_line);

                    var temp_light_line = new LightLine(self, fp1, fp2);

                    if (testAgainstLines(temp_light_line) && testAgainstElements(temp_light_line)) {
                        f_temp_line = new fabric.Line([fp1.x, fp1.y, fp2.x, fp2.y], {
                            stroke: drawingStyle.temp_line_color,
                            strokeWidth: drawingStyle.temp_line_width,
                            selectable: false
                        });
                    }
                    else {
                        f_temp_line = new fabric.Line([fp1.x, fp1.y, fp2.x, fp2.y], {
                            stroke: drawingStyle.temp_line_color,
                            strokeWidth: drawingStyle.temp_line_width,
                            selectable: false,
                            strokeDashArray: [5, 5]
                        });
                    }

                    canvas.add(f_temp_line);
                }
            }
        };
        mouseUp["mouse_key:line_drawing"] = function (options) {

            is_mouse_down = false;
            fp2 = canvas.getPointer(options.e);
            fp2 = snapToGrid(fp2);
            canvas.remove(f_temp_line);

            if (fp2) {                                                
                if (!samePoint(fp1, fp2)) {
                    canvas.renderOnAddRemove = false;
                    //console.time("from renderOnAddRemove = false to renderOnAddRemove = true");
                    addElement(new Line(drawing, fp1, fp2));
                    //console.timeEnd("from renderOnAddRemove = false to renderOnAddRemove = true");
                    canvas.renderOnAddRemove = true;
                    flipFlop();

                } else {
                    fp1 = null;
                }

            } else {
                fp1 = null;
            }
        };

        mouseDown["mouse_key:impedance_drawing"] = function (options) {
            var point = canvas.getPointer(options.e);

            var dummy_rectangle = snapToLine(point);

            if (dummy_rectangle) {
                dummy_rectangle.cellSize = cellSize;
                addElement(new Impedance(dummy_rectangle));
            }
        };
        mouseMove["mouse_key:impedance_drawing"] = function (options) { };
        mouseUp["mouse_key:impedance_drawing"] = function (options) { };

        mouseDown["mouse_key:erasing"] = function (options) {
            var point = canvas.getPointer(options.e);

            var dummy_rectangle = snapToLine(point);

            if (dummy_rectangle) {
                var candidate_line = dummy_rectangle.parent_line,
                    critical_point = dummy_rectangle.center, 
                    line_to_be_deleted = candidate_line.checkoutLinesOnErasing(critical_point);

                if (line_to_be_deleted) {
                    line_to_be_deleted.unRender(canvas, lines);
                    return;
                }

                var rectangle_to_be_deleted = candidate_line.checkoutRectanglesOnErasing(critical_point);

                if (rectangle_to_be_deleted) {
                    var new_line = new Line(self, candidate_line.fp1, candidate_line.fp2);

                    candidate_line.elements.forEach(function (element, index, array) {
                        if (element instanceof Rectangle && element !== rectangle_to_be_deleted) {
                            var new_rectangle = element.clone();
                            new_rectangle.parent_line = new_line;
                            new_line.addElementToSelf(new_rectangle, "no render");
                        }
                    });

                    candidate_line.unRender(canvas, lines);
                    new_line.render(canvas, lines);
                }
            }

        };
        mouseMove["mouse_key:erasing"] = function (options) { };
        mouseUp["mouse_key:erasing"] = function (options) { };

        canvas.on('mouse:down', function (options) {
            mouseDown[self.mouse_key](options);
        });
        canvas.on('mouse:move', function (options) {
            mouseMove[self.mouse_key](options);
        });
        canvas.on('mouse:up', function (options) {
            mouseUp[self.mouse_key](options);
        });
    };

    inheritElements = function (heir, predecessor) {

        var saved_mouse_key = self.mouse_key;

        $("body").css("pointer-events", "none");
        self.mouse_key = "mouse_key:impedance_drawing"; // ovaj key ce se mozda zvati drugacije!!!

        predecessor.activate();
        predecessor.deActivate("last deactivation");
        heir.activate();
        heir.deActivate("last deactivation");

        predecessor.elements.forEach(function (item, index, array) {

            item.activate();
            item.deActivate("last deactivation");

            if (item instanceof Rectangle && heir.contains(item.center)) {
                item.parent_line = heir;
                var clone = item.clone();
                addElement(clone, "no render");
            }
        });

        self.mouse_key = saved_mouse_key;
        $("body").css("pointer-events", "auto");
    }
    addElement = function (element, no_render) {
        add["element_key:line"] = function (new_line) {

            var possible = testAgainstLines(new_line) && testAgainstElements(new_line);

            if (possible) {
                var noodles = new_line.selfDivideByEndPoints(lines);
                if (noodles.length !== 0) {
                    noodles.forEach(function (noodle, index, array) {
                        addElement(noodle);
                    });
                    return;
                }

                var orphans = new_line.divideVictims(lines);
                if (orphans.length !== 0) {
                    orphans.forEach(function (orphan, index, array) {
                        removeElement(orphan.victim);
                        inheritElements(orphan, orphan.victim);
                        addElement(orphan);
                    });
                    addElement(new_line);
                    return;
                }

                var siblings = new_line.findSiblings(lines);
                if (siblings.length !== 0) {

                    siblings.forEach(function (sibling, index, array) {
                        removeElement(sibling);
                    });

                    var big_line = new_line.makeBigLine(siblings, inheritElements);                    
                    addElement(big_line);

                    return;
                }

                new_line.render(canvas, lines);
                cleanUp(lines);
                
            } else {
                console.log("line NOT added ");
            }          
        };
        add["element_key:rectangle"] = function (rectangle, no_render) {

            var parent_line = rectangle.parent_line;

            if (!parent_line.accepts(rectangle))
                return;

            if (!drawingAccepts(rectangle))
                return;

            parent_line.addElementToSelf(rectangle, no_render);

            console.log('adding rectangle'); 
        };

        add[element.element_key](element, no_render);
        f_temp_line = null;
    };
    removeElement = function (element) {
        remove["element_key:line"] = function (line) {

            line.unRender(canvas, lines);
            
        };

        remove[element.element_key](element);
    };

    drawGrid();
    initMouseHandlers();
}

